# Системы управления базами данных


## ссылки

 * https://bitbucket.org/bskydive/bookstore-server/src/master/bookStoreServer/sql/
 * [транзакционность DDL](https://wiki.postgresql.org/wiki/Transactional_DDL_in_PostgreSQL:_A_Competitive_Analysis)
 * [про базы данных от динозавров](https://stackoverflow.com/users/484814/performancedba)
 * JOIN
	* [Сделай сам: SQL JOIN на Java](https://habrahabr.ru/post/278087/)
	* [Понимание джойнов сломано. Это точно не пересечение кругов, честно](https://habr.com/ru/post/448072/)
 * транзакционность
	* [Уровни изоляции транзакций с примерами на PostgreSQL](https://habr.com/ru/articles/317884)
	* [уровни изоляции транзакций postgrespro](https://postgrespro.ru/docs/postgrespro/16/transaction-iso)
	* [На пути к правильным SQL транзакциям (Часть 1) 2015](https://habr.com/ru/companies/infopulse/articles/261097/)
 * [10 потенциальных SQL ошибок, которые делают программисты](https://habrahabr.ru/post/208264/)
 * справочники команд sql
	* https://sql-academy.org/ru/handbook
	* http://www.w3schools.com/sql/default.asp
	* http://wiki.mvtom.ru/index.php/Структурированный_язык_запросов_SQL/
 * тренажёры
	* https://sql-academy.org/ru/trainer
	* [sql-ex](http://sql-ex.ru)
 * Учебники
	* https://postgrespro.ru/docs/postgresql/16/sql-syntax-lexical.html#sql-syntax-identifiers
	* http://www.w3schools.com/sql/sql_func_count.asp
	* https://www.codecademy.com/en/courses/learn-sql/
	* https://scanlibs.com/page/6/?s=oracle
	* [SQL Server Sample Database](https://www.sqlservertutorial.net/getting-started/sql-server-sample-database/)
	* https://www.freecodecamp.org/news/a-beginners-guide-to-sql

## ERD 

 * https://www.lucidchart.com/pages/er-diagrams
 * Entity - прямоугольник, сущность
	* entity type - группа сущностей
	* entity set - тип, ограниченный во времени
	* entity category
		* strong - определяется свойствами одной сущности
		* weak - определяется свойствами нескольких сущностей
		* associative - связи внутри set
		* ![](./СУБД/entity_type.png)
 * relation
	* ![](./СУБД/rel_cardinality.jpeg)
 * notation
	* ![](./СУБД/erd_notation_martin.jpeg)
	* ![](./СУБД/erd_notation_barker.jpeg)
 * виды моделей
    * Conceptual - overall scope of the model and portraying the system architecture
    * Logical - More detailed operational and transactional entities are now defined. The logical model is independent of the technology in which it will be implemented.
    * Physical - One or more physical model may be developed from each logical model. The physical models must show enough technology detail to produce and implement the actual database.
 * https://www.freecodecamp.org/news/database-normalization-1nf-2nf-3nf-table-examples/
	* регулируют избыточность
	* PK - uniq
	* FK
	* Composite - unique, multiple columns
	* 1 - 
		* a single cell must not hold more than one value (atomicity)
		* there must be a primary key for identification
		* no duplicated rows or columns
		* each column must have only one value for each row in the table
	* 2 - has no partial dependency. That is, all non-key attributes are fully dependent on a primary key
	* 3 - have no transitive partial dependency
	* 4 - 
	* 5 - 
	* 6 - 

## инструменты

 * облачные IDE
	* http://sqlfiddle.com
 * sql execution plan
	* https://livesql.oracle.com/apex/livesql/file/index.html
 * тестирование
	* [доступность](./доступность.md)
	* производительность
		* [YDB знакомится с TPC-C: раскрываем производительность наших распределенных транзакций](https://habr.com/ru/companies/ydb/articles/763938/)
		* https://www.tpc.org/tpcc/
		* https://github.com/cmu-db/benchbase
		* https://github.com/yugabyte/tpcc/
	* согласованность

## Типы СУБД

 * ![](./СУБД/database_type.jpg)
 * https://db-engines.com/en/ranking
 * https://sql-workbench.eu/dbms_comparison.html
 	* ![](./СУБД/сравнение_субд.png)
 * реляционные/универсальные
	* преимущества
		* универсальность
		* очень большой запас инструментов масштабирования - от настроек до смены движка одной строчкой в конфиге
        * индексы и связи, логика объединения данных и запросов
	* недостатки
		* сложны в изучении и применении на больших объёмах данных
		* много компромиссов из-за универсальности и поддержки legacy структур/языков/интерфейсов
	* PostgreSQL
	* MySQL
	* Oracle
	* DB2
	* MS SQL Server
 * документоориентированные
	* преимущества
        * распределённые, легко масштабируются
		* хранят данные в одном документе JSON
		* DDD документо/предметно - ориентированный подход
	* недостатки
		* изменения в бизнес-логике порождают изменения в структуре/модели документов/данных, это ломает DDD изоляцию
	* MongoDB
	* Apache CouchDB
	* Couchbase Server
	* MarkLogic
	* Amazon dynamoDB
 * Графовые базы данных
	* Neo4j
	* AllegroGraph
	* Amazon Neptune
	* JanusGraph
	* Dgraph
 * ГИС/spatial
	* PostGIS
	* OracleSpatial
	* MS SQL Server spatial
 * Хранилища "ключ-значение". простая модель данных, которая легко масштабируется
    * индексирование и поиск по ключам
	* Redis
	* memcache
	* Aerospike
	* Amazon DynamoDB
 * [колоночные/columnar базы данных](https://www.integrate.io/blog/whats-unique-about-a-columnar-database/)
    * индексирование и сжатие по столбцам
	* вертикальное шардирование по столбцам/колонкам для OLAP нагрузок
	* [Amazon redshift](https://aws.amazon.com/ru/redshift/?nc=sn&loc=1)
	* Cassandra
	* Google Bigtable
	* [Apache HBase](https://hbase.apache.org/)
	* [Clickhouse](https://clickhouse.com/)
		* ![](./СУБД/clickhouse_arch.jpg)
		* [ClickHouse: полезные лайфхаки 2023](https://habr.com/ru/articles/743772/)
		* [ClickHouse](https://yandex.ru/dev/clickhouse/)
		* [ClickHouse: очень быстро и очень удобно 2017](https://habr.com/ru/articles/322724/)
		* []()
	* [snowflake](https://docs.snowflake.com/en/user-guide/intro-key-concepts)
	* ScyllaDB
	* Кластеризованные MPP - Massive Parallel Processing, DWH data warehouse
		* [apache hadoop hive](https://hive.apache.org/)
			* ![](./СУБД/hive_mps.jpg)
		* [greenplum](https://greenplum.org/tutorials/#core)
			* https://docs.vmware.com/en/VMware-Greenplum/7/greenplum-database/admin_guide-intro-arch_overview.html
			* ![](./СУБД/greenplum_arch.jpg)
 * [NewSQL: SQL никуда не уходит 2018](https://habr.com/ru/companies/oleg-bunin/articles/413557/)
	* преимущества
		* inmemory
		* mission critical: горизонтальное масштабирование без потери консистентности(serializable) и производительности
		* новые модели данных - документы, графы
 	* CockroachDB - Postgres
	* VoltDB
	* NuoDB
	* Google cloud spanner
	* TiDB - mysql
	* YugabyteDB - postgres
	* [YDB](https://cloud.yandex.com/ru/docs/ydb/concepts/)
	* mysql Percona Server(NDB Cluster + TokuDB)
	* 
 * полнотекстовые
	* Elasticsearch
	* Sphinx
 * control-plane
	* ZooKeeper
	* Consul
	* Etcd
 * time-series. Оптимизированы на запись и выборку по времени
    * индексирование и поиск по времени/дате
	* InfluxDB
	* TimeScaleDB
	* Amazon Timestream

## проектирование схемы данных

 * https://www.slideshare.net/maieutic/ss-390247
 * [Проектирование баз данных denis.beskov](http://database-design.livejournal.com/)
 * [Data Models](http://www.databaseanswers.org/data_models/)

## MySQL

* https://stackoverflow.com/questions/41887460/select-list-is-not-in-group-by-clause-and-contains-nonaggregated-column-inc

```sql
SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

```

## PoestreSQL



## SQL

 * [examples.sql](./СУБД/examples.sql.md)

 * [ISO/IEC 9075-2023](https://blog.ansi.org/sql-standard-iso-iec-9075-2023-ansi-x3-135/)
	* ISO/IEC 9075-1:2023 - Framework (SQL/Framework)
	* ISO/IEC 9075-2:2023 - Foundation (SQL/Foundation)
	* ISO/IEC 9075-3:2023 - Call-Level Interface (SQL/CLI)
	* ISO/IEC 9075-4:2023 - Persistent stored modules (SQL/PSM)
	* ISO/IEC 9075-9:2023 - Management of External Data (SQL/MED)
	* ISO/IEC 9075-10:2023 - Object language bindings (SQL/OLB)
	* ISO/IEC 9075-11:2023 - Information and definition schemas (SQL/Schemata)
	* ISO/IEC 9075-13:2023 - SQL Routines and types using the Java TM programming language (SQL/JRT)
	* ISO/IEC 9075-14:2023 - XML-Related Specifications (SQL/XML)
 * https://learnsql.com/blog/standard-sql-functions-cheat-sheet/
 * https://learnsql.com/blog/sql-string-functions/
 * https://www.postgresql.org/docs/current/plpgsql-control-structures.html
 * https://www.postgresql.org/docs/current/arrays.html
 * https://www.postgresql.org/docs/current/functions-matching.html
 * https://www.mssqltips.com/sqlservertip/7164/sql-while-loop-examples-alternatives/
 * https://sql-academy.org/ru/guide/operator-regexp

 * https://dev.mysql.com/doc/refman/9.0/en/differences-from-ansi.html
 * https://wiki.postgresql.org/wiki/PostgreSQL_vs_SQL_Standard

### Оптимизация

 * https://www.geeksforgeeks.org/order-of-execution-of-sql-queries/
 * https://blog.jooq.org/a-beginners-guide-to-the-true-order-of-sql-operations/
 * [порядок выполнения операторов](https://learnsql.com/blog/sql-order-of-operations/) logical order of operations
	* from
	* join
	* where фильтр перед группировкой
	* group by
	* aggregations
	* having фильтр после группировки
	* select
	* distinct
	* UNION, INTERSECT, EXCEPT
	* order by
	* offset
	* limit
 * optimization
	* Properly index columns used in WHERE and JOIN conditions.
	* Use appropriate data types and avoid unnecessary data type conversions.
	* Limit the use of SELECT * and only retrieve the columns you need.
	* Minimize the use of subqueries and consider JOIN alternatives.
	* Monitor and analyze query performance using database-specific tools and profiling.

## репликация

 * [integrate.io binlog Replication](https://www.integrate.io/docs/cdc/)
	* https://www.integrate.io/docs/cdc/postgresql/
	* https://www.integrate.io/docs/cdc/elt-cdc-features-and-limitations/
 * [Debezium — CDC (Capture Data Change), набор коннекторов для различных СУБД, совместимых с фреймворком Apache Kafka Connect](https://debezium.io/) - для стриминга данных
	* [Знакомство с Debezium — CDC для Apache Kafka 2020](https://habr.com/ru/companies/flant/articles/523510/)
	* ![](./СУБД/debezuim_arch.jpg)

### шардинг

 * ключ
 * хэш
 * диапазон
 * каталог/файл
 * вертикальный - по столбцам
 * горизонтальный - по строкам

### инструменты определения требований к задержке в сети

 * [RTT, RTO, RPO и синхронная репликация — Павел Конотопов, PGConf.Russia 2023](https://www.youtube.com/watch?v=n8LLsbqEs_I)
 * pg_bench
 * [go-tpc - benchmark workloads in TPC for databases](https://github.com/pingcap/go-tpc)
 * [comcast - simulate common network problems like latency, bandwidth restrictions, and dropped/reordered/corrupted packets](https://github.com/tylertreat/comcast)
 * haproxy
 * [owamp - validate the RPO, RTO of a PostgreSQL HA instance](https://github.com/perfsonar/owamp)
	* [RFC 4656 A One-way Active Measurement Protocol (OWAMP)](https://www.protokols.ru/WP/rfc4656/)
 * ptor

## Производительность, оптимизация

 * [The DB-Engines Ranking ranks database management systems according to their popularity](https://db-engines.com/en/ranking)
 * 

### postgresql

 * [ Тестирование производительности кластеров PostgreSQL на различных аппаратных платформах Postgres Professional 2024](https://www.youtube.com/watch?v=PvlRsfnTOEA)
 * ![](./СУБД/postgres_performance_select.jpg)
 * ![](./СУБД/postgres_performance_update.jpg)
 * ![](./СУБД/postgres_performance_config.jpg)
 * ![](./СУБД/postgres_performance_pro.jpg)
 * vacuumdb
 * pgperf
 * https://www.postgresql.org/docs/16/limits.html
 * ![](./СУБД/postgres_limits.jpg)

### mysql

 * https://www.oreilly.com/library/view/high-performance-mysql/9781449332471/ch01.html
 * https://en.wikipedia.org/wiki/Comparison_of_MySQL_database_engines
 * основной движок InnoDB, для полнотекстового поиска myISAM
 * https://dev.mysql.com/doc/mysql-shell/8.4/en/innodb-clusterset.html
 * https://www.percona.com/blog/innodb-performance-optimization-basics-updated
	* (Dynamic) - Does not require MySQL restart for change.
	* (Static) - Requires MySQL restart for change.
	* innodb_buffer_pool_size (Dynamic) - 70%-80% of available memory
	* innodb_buffer_pool_instances  (Static) - find optimal starting form 8
	* innodb_buffer_pool_chunk_size  (Static) - Refer to InnoDB Buffer Pool Resizing: Chunk Change for more details on configuration.
	* innodb_redo_log_capacity (Dynamic) - This variable supersedes the innodb_log_files_in_group and innodb_log_file_size variables. When this setting is defined, the innodb_log_files_in_group and innodb_log_file_size settings are ignored (those two variables are now deprecated since 8.0.30).
	* innodb_log_buffer_size (Dynamic) - If you have transactions that update, insert, or delete many rows, making the log buffer larger saves disk I/O. 
	* innodb_flush_log_at_trx_commit (Dynamic) - The default value of 1 gives the most durability (ACID compliance) at a cost of increased filesystem writes/syncs. Setting the value to 0 or 2 will give more performance but less durability. At a minimum, transactions are flushed once per second.
	* innodb_flush_method (Static) - Setting this to O_DIRECT will avoid a performance penalty from double buffering;

## теория

### индексы

 * индексы в БД по-умолчанию древовидные
 * составные индексы используются не могут использоваться для поиска без первого поля, т.к. вторичный индекс строится от первичного
 * индексы бесполезны для столбцов с простыми типами-словарями
 * индексы хранятся в отдельном файле
 * индексы могут перестраиваться постепенно

### уровни изоляции транзакций

 * [BASE/ACID](./доступность.md#base-vs-acid)
 * побочные эффекты:
	* Потерянное обновление (lost update) - изменения, сделанные одной транзакцией, затираются другой
	* «грязное» чтение - dirty read
		* Транзакция читает данные, записанные параллельной незавершённой транзакцией.
	* неповторяемое чтение - non-repeatable read
		* Транзакция повторно читает те же(свои) данные, что и раньше, и обнаруживает, что они были изменены другой транзакцией (которая завершилась после первого чтения).
	* фантомное чтение - phantom reads
		* Транзакция повторно выполняет запрос, возвращающий набор строк для некоторого условия, и обнаруживает, что набор строк, удовлетворяющих условию, изменился из-за транзакции, завершившейся за это время.
	* аномалия сериализации
		* Результат успешной фиксации группы транзакций оказывается несогласованным при всевозможных вариантах исполнения этих транзакций по очереди.
 * ![](./СУБД/sql_изоляция_транзакций.jpg)
 * [Уровни изоляции транзакций с примерами на PostgreSQL 2016](https://habr.com/ru/articles/317884/)
 * https://postgrespro.ru/docs/postgrespro/16/transaction-iso

### Задачи

 * [вложенные запросы с LATERAL](https://habr.com/ru/company/tensor/blog/574330/)
 * https://proglib.io/p/sql-questions/
 * https://habrahabr.ru/post/181033/

```sql
	create table department;
	create table employee;
	commit;


	Insert into DEPARTMENT
	(ID, NAME)
	Values
	(1, 'Руководство');
	Insert into DEPARTMENT
	(ID, NAME)
	Values
	(2, 'Секритариат');
	Insert into DEPARTMENT
	(ID, NAME)
	Values
	(3, 'IT');
	Insert into DEPARTMENT
	(ID, NAME)
	Values
	(4, 'Хоз.Отдел');
	COMMIT;

	Insert into EMPLOYEE
	(ID, DEPARTMENT_ID, NAME, CHIEF_ID, SALARY)
	Values
	(1, 1, 'Иванов И.И.', NULL, 100000);
	Insert into EMPLOYEE
	(ID, DEPARTMENT_ID, NAME, CHIEF_ID, SALARY)
	Values
	(2, 1, 'Петров П.И.', 1, 90000);
	Insert into EMPLOYEE
	(ID, DEPARTMENT_ID, NAME, CHIEF_ID, SALARY)
	Values
	(3, 1, 'Сидоров А.Л.', 1, 95000);
	Insert into EMPLOYEE
	(ID, DEPARTMENT_ID, NAME, CHIEF_ID, SALARY)
	Values
	(4, 2, 'Смирнова О.П.', 1, 20000);
	Insert into EMPLOYEE
	(ID, DEPARTMENT_ID, NAME, CHIEF_ID, SALARY)
	Values
	(5, 3, 'Константинов Л.И.', 2, 80000);
	Insert into EMPLOYEE
	(ID, DEPARTMENT_ID, NAME, CHIEF_ID, SALARY)
	Values
	(6, 3, 'Федоров С.И.', 5, 100000);
	Insert into EMPLOYEE
	(ID, DEPARTMENT_ID, NAME, CHIEF_ID, SALARY)
	Values
	(7, 3, 'Самойлов Н.И.', 5, 100000);
	Insert into EMPLOYEE
	(ID, DEPARTMENT_ID, NAME, CHIEF_ID, SALARY)
	Values
	(8, 3, 'Никифоров С.П.', 7, 20000);
	Insert into EMPLOYEE
	(ID, DEPARTMENT_ID, NAME, CHIEF_ID, SALARY)
	Values
	(9, 4, 'Митрофанова М.С.', 3, 10000);
	COMMIT;
```


