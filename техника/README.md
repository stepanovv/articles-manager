# Теория

 * [алгоритмы](./алгоритмы.md)
 * [Анализ данных](./анализ_данных.md)
 * [архитектура](./архитектура.md)
 * [доступность](./доступность.md)
 * [защита](./защита.md)
 * [нейронка](./нейронка.md)
 * [проектирование](./проектирование.md)
 * [производительность](./производительность.md)
 * [сети](./сети.md)
 * [справочная](./справочная.md)
 * [СУБД](./СУБД.md)
 * [типовые_проекты](./типовые_проекты.md)
 * [хранилище](./хранилище.md)
 * [шаблоны](./шаблоны.md)


## TODO

Кевин Митник - The Art of Deception Искусство обмана 2004, FB2, RUS
[параллельные конкурентные потоки многопоточность](https://hacks.mozilla.org/2017/06/a-crash-course-in-memory-management/) @книга
Стив Макконелл
[Лекция 4 в НГУ: DDD на примерах](https://www.youtube.com/watch?v=PQq37uegigo) @видео
[Лекция 2 в НГУ: погружение в архитектуру](https://www.youtube.com/watch?v=fy4DSOG8kas) @видео
[Лекция 6 в НГУ: Продуктовая разработка](https://www.youtube.com/watch?v=40cD6AbIVhY) @видео
[DWH без CDI — деньги на ветер что Сравни узнали о DWH при внедрении CDI](https://www.youtube.com/watch?v=F4JbML1Q8ss) @видео
[Лекция 11 в НГУ: как пройти интервью по проектированию систем (практика)](https://www.youtube.com/watch?v=zY6v-Cko3b8) @видео
[Git Flow Is A Bad Idea](https://www.youtube.com/watch?v=_w6TwnLCFwA) @видео
[Лекция 10 в НГУ: Docker и Kubernetes](https://www.youtube.com/watch?v=rYF_XyUB53s) @видео
