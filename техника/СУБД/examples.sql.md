# SQL

## сортировка по убыванию численности компании

```sql
SELECT
  id,
  ceo,
  motto,
  employees
FROM companies 
ORDER BY cast(employees as integer) DESC;
-- ORDER BY employees DESC;
-- ORDER BY (employees + 0) DESC;
```

## арифметические операции

```sql
SELECT 
  a, 
  b, 
  (180 - cast(a AS integer) - cast(b AS integer)) AS res
--   180 - (a + b) AS res
FROM otherangle;
```


 * POW(num, power)	Вычисляет число в указанной степени
 * SQRT(num)	Вычисляет квадратный корень числа
 * LOG(base, num)	Вычисляет логарифм числа по указанному основанию
 * EXP(num)	Вычисляет enum
 * SIN(num)	Вычисляет синус числа
 * COS(num)	Вычисляет косинус числа
 * TAN(num)	Вычисляет тангенс числа
 * Округление
	* CEIL в меньшую до целого
	* FLOOR в большую до целого

TRUNCATE отбрасывает ненужные цифры.

```sql
TRUNCATE(69.7979,1)	69.7
TRUNCATE(69.7979,2)	69.79
TRUNCATE(69.7979,3)	69.797

ROUND в ближайшую (0.5) до целого

округлять число до некоторой части десятичных знаков после запятой.

ROUND(69.7171,1)	69.7
ROUND(69.7171,2)	69.72
ROUND(69.7171,3)	69.717

Второй аргумент функции ROUND может принимать также и отрицательные значения. 
В этом случае, цифры слева от десятичной точки числа становятся равными нулю на указанное в аргументе количество, а дробная часть обрезается.

ROUND(1691.7,-1)	1690
ROUND(1691.7,-2)	1700
ROUND(1691.7,-3)	2000

SIGN возвращает значение -1, если число отрицательно, 0, если число нулевое и 1, если число положительное.
ABS возвращает абсолютное значение числа.

*/
```

## циклы


 * последовательность умножений 1..n на x

```sql

CREATE OR REPLACE FUNCTION count_by_multiples(x INT, n INT)
RETURNS INT[] AS $$
DECLARE
    multiples INT[];
BEGIN
    multiples := ARRAY(SELECT x * i FROM generate_series(1, n) AS i);
    RETURN multiples;
END;
$$ LANGUAGE plpgsql;

SELECT
    x,
    n,
    count_by_multiples(x, n) AS res
FROM
    counter
ORDER BY
    x ASC,
    n ASC;

--

SELECT x, n, ARRAY(SELECT generate_series(x, x*n, x)) AS res
FROM counter
ORDER BY x, n
```

## regexp

```sql

SELECT name, phone_number FROM Users WHERE phone_number REGEXP '^\\+7'
SELECT * FROM Users WHERE phone_number REGEXP '^[^28]*$'
SELECT * FROM Users WHERE email REGEXP '@(outlook.com|icloud.com)$'
SELECT * FROM Users WHERE name REGEXP '^John'
```

## группировка

 * https://www.baeldung.com/sql/group-by-vs-distinct
	* distinct только фильтрует, gorupby подготавливает для агрегирования функциями

```sql

SELECT home_type, MIN(price) as min_price 
FROM Rooms
WHERE has_tv = True
GROUP BY home_type
HAVING COUNT(*) >= 5;

SELECT home_type, (MAX(price) - MIN(price)) as difference 
FROM Rooms
GROUP BY home_type
HAVING COUNT(*) >= 2;
```

## связывание JOIN

 * https://learnsql.com/blog/sql-joins-complete-guide/
 * https://learnsql.com/blog/illustrated-guide-sql-outer-join/
 * https://learnsql.com/blog/illustrated-guide-sql-non-equi-join/
 * https://learnsql.com/blog/how-to-join-3-tables-or-more-in-sql/
	* junction table - purpose of this table is to connect tables

```sql

SELECT family_member, member_name FROM Payments
INNER JOIN FamilyMembers
    ON Payments.family_member = FamilyMembers.member_id
-- эквивалент
SELECT family_member, member_name FROM Payments, FamilyMembers
    WHERE Payments.family_member = FamilyMembers.member_id

-- INNER
SELECT Class.name, Student_in_class.student FROM Class
INNER JOIN Student_in_class
    ON Class.id = Student_in_class.class;

-- эмуляция FULL в mysql

SELECT *
FROM левая_таблица
LEFT JOIN правая_таблица
   ON правая_таблица.ключ = левая_таблица.ключ

UNION ALL

SELECT *
FROM левая_таблица
RIGHT JOIN правая_таблица
ON правая_таблица.ключ = левая_таблица.ключ
 WHERE левая_таблица.ключ IS NULL

-- количество связанных с учителем записей, NULL заменён на 0

SELECT Teacher.first_name, Teacher.last_name, COUNT(Schedule.id) AS amount_classes
FROM Teacher
LEFT JOIN Schedule
ON Teacher.id = Schedule.teacher
GROUP BY Teacher.id
UNION ALL
SELECT Teacher.first_name, Teacher.last_name, COUNT(Schedule.id) AS amount_classes
FROM Teacher
RIGHT JOIN Schedule
ON Teacher.id = Schedule.teacher
WHERE Schedule.teacher IS NULL
GROUP BY Teacher.id;

-- вывести имена купивших два разных товара одновременно в марте

SELECT Customer.name
FROM Customer 
JOIN Purchase ON Purchase.customer_key = Customer.customer_key
JOIN Product ON Product.product_key = Purchase.product_key
WHERE 
(MONTH(Purchase.date) = 3) AND
(YEAR(Purchase.date) = 2024) AND
Product.name IN ('Monitor', 'Laptop')
GROUP BY Customer.customer_key
HAVING COUNT(DISTINCT Product.name) > 1

-- все данные из левой, дополненные данными из правой

SELECT поля_таблиц 
FROM левая_таблица LEFT JOIN правая_таблица 
    ON правая_таблица.ключ = левая_таблица.ключ 

-- все данные из правой, дополненные данными из левой

SELECT поля_таблиц
FROM левая_таблица RIGHT JOIN правая_таблица
    ON правая_таблица.ключ = левая_таблица.ключ

-- данные только из левой(нет парных данных в правой)

SELECT поля_таблиц
FROM левая_таблица LEFT JOIN правая_таблица
    ON правая_таблица.ключ = левая_таблица.ключ
WHERE правая_таблица.ключ IS NULL

-- данные только из правой(нет парных данных в левой)

SELECT поля_таблиц
FROM левая_таблица RIGHT JOIN правая_таблица
    ON правая_таблица.ключ = левая_таблица.ключ
WHERE левая_таблица.ключ IS NULL

-- только связанные данные(есть парные в левой и правой)

SELECT поля_таблиц
FROM левая_таблица INNER JOIN правая_таблица
    ON правая_таблица.ключ = левая_таблица.ключ

-- все данные в двух таблицах, парные и не парные

SELECT поля_таблиц 
FROM левая_таблица
    FULL OUTER JOIN правая_таблица
    ON правая_таблица.ключ = левая_таблица.ключ

-- не парные данные из левой, плюс не парные данные из правой (обратное INNER JOIN)

SELECT поля_таблиц 
FROM левая_таблица
    FULL OUTER JOIN правая_таблица
    ON правая_таблица.ключ = левая_таблица.ключ
WHERE левая_таблица.ключ IS NULL
    OR правая_таблица.ключ IS NULL
```

## LIMIT

```sql

SELECT * FROM Company LIMIT 2, 3;
SELECT * FROM Company LIMIT 3 OFFSET 2;
```

## скалярные подзапросы

```sql

SELECT (SELECT name FROM company LIMIT 1) AS company_name;
SELECT * FROM FamilyMembers
    WHERE birthday = (SELECT MAX(birthday) FROM FamilyMembers);
SELECT * FROM Reservations
    WHERE Reservations.room_id = (
        SELECT id FROM Rooms ORDER BY price DESC LIMIT 1
    )

-- свертывание в скаляр reduce: IN/ALL/ANY

SELECT DISTINCT name FROM Users INNER JOIN Rooms
    ON Users.id = Rooms.owner_id
    WHERE Users.id <> ALL (
        SELECT DISTINCT user_id FROM Reservations
    )

SELECT * FROM Users 
	WHERE id = 
	ANY (SELECT DISTINCT owner_id FROM Rooms WHERE price >= 150)

SELECT * FROM Users 
	WHERE id 
	IN (SELECT DISTINCT owner_id FROM Rooms WHERE price >= 150)

SELECT good_name FROM Goods 
	WHERE Goods.good_id 
	NOT IN (SELECT Payments.good FROM Payments);

SELECT * FROM Rooms 
	WHERE (has_tv, has_internet, has_kitchen, has_air_con) 
	IN (SELECT has_tv, has_internet, has_kitchen, has_air_con FROM Rooms WHERE id = 11);
```

## Коррелированные подзапросы

```sql

SELECT FamilyMembers.member_name, 
	(SELECT SUM(Payments.unit_price * Payments.amount) FROM Payments
    WHERE Payments.family_member = FamilyMembers.member_id
	) AS total_spent
FROM FamilyMembers;

SELECT member_name, 
	(SELECT MAX(Payments.unit_price) 
	FROM Payments 
	WHERE Payments.family_member = FamilyMembers.member_id) 
	AS good_price 
FROM FamilyMembers;
```

## Обобщённое табличное выражение или CTE (Common Table Expressions)

```sql

WITH Aeroflot_trips AS
    (SELECT TRIP.* FROM Company
        INNER JOIN Trip ON Trip.company = Company.id WHERE name = "Aeroflot")

SELECT plane, COUNT(plane) AS amount FROM Aeroflot_trips GROUP BY plane;

WITH Aeroflot_trips AS
    (SELECT TRIP.* FROM Company
        INNER JOIN Trip ON Trip.company = Company.id WHERE name = "Aeroflot"),
    Don_avia_trips AS
    (SELECT TRIP.* FROM Company
        INNER JOIN Trip ON Trip.company = Company.id WHERE name = "Don_avia")

SELECT * FROM Don_avia_trips UNION SELECT * FROM  Aeroflot_trips;

```

## UNION


UNION по умолчанию убирает повторения в результирующей таблице. Для отображения с повторением есть необязательный параметр ALL
Не путайте операции объединения запросов с JOIN и подзапросами. Они выполняются для связанных таблиц.

Для корректной работы UNION нужно, чтобы результирующие таблицы каждого из SQL запросов имели одинаковое число столбцов, с одним и тем же типом данных и в той же самой последовательности.

Существует два других оператора, чьё поведение крайне схоже с UNION:

 * INTERSECT Комбинирует два запроса SELECT, но возвращает записи только первого SELECT, которые имеют совпадения во втором элементе SELECT.
 * EXCEPT Комбинирует два запроса SELECT, но возвращает записи только первого SELECT, которые не имеют совпадения во втором элементе SELECT.

```sql
SELECT good_type_name
FROM GoodTypes AS NotG
WHERE NOT EXISTS (
		SELECT DISTINCT good_type_name
		FROM Payments,
			Goods,
			GoodTypes
		WHERE good_id = good
			AND good_type_id = type
			AND YEAR(date) = 2005
			AND good_type_name = NotG.good_type_name
	)
```

## Условия

```sql

SELECT first_name, last_name,
CASE
  WHEN TIMESTAMPDIFF(YEAR, birthday, NOW()) >= 18 THEN "Совершеннолетний"
  ELSE "Несовершеннолетний"
END AS status
FROM Student

SELECT Reviews.id, 
CASE 
WHEN Reviews.rating > 3 AND Reviews.rating < 6 THEN 'positive'
WHEN Reviews.rating = 3 THEN 'neutral'
WHEN Reviews.rating < 3 AND Reviews.rating > 0 THEN 'negative'
ELSE 'undefined'
END AS rating
FROM Reviews;

SELECT id, price,
    IF(price >= 150, "Комфорт-класс", "Эконом-класс") AS category
    FROM Rooms

SELECT id, price,
    IF(price >= 200, "Бизнес-класс",
        IF(price >= 150,
            "Комфорт-класс", "Эконом-класс")) AS category
    FROM Rooms

SELECT id, IF(has_tv = TRUE, 'YES', 'NO') AS has_tv FROM Rooms;

IFNULL(значение, альтернативное_значение);
NULLIF(значение_1, значение_2);
-- Функция NULLIF возвращает NULL, если значение_1 равно значению_2, в противном случае возвращает значение_1
```

### умножение по условию

```sql
SELECT number, 
  CASE
    WHEN number % 2 = 0 
		THEN number*8
    	ELSE number*9
  END AS res 
FROM multiplication;
```

## фильтр

### boolean

```sql
SELECT
  name,
  age,
  semester,
  mentor,
  tuition_received
FROM students
WHERE tuition_received <> true;
-- WHERE tuition_received = false;
-- WHERE NOT tuition_received;
```

## Модификация

### INSERT

```sql

INSERT INTO Goods (good_id, good_name, type)
SELECT 20, 'Table', 2;

INSERT INTO Goods (good_id, good_name, type)
VALUES (20, 'Table', 2);

CREATE TABLE Goods (good_id SERIAL); -- Pgsql
CREATE TABLE Goods (good_id INT NOT NULL AUTO_INCREMENT); --Mysql
INSERT INTO Goods VALUES (NULL, 'Table', 2); -- у AUTO_INCREMENT автоматическое приведение типов NULL-->INT

INSERT INTO Goods (
		good_id, 
		good_name, 
		type)
	SELECT 
		(SELECT COUNT(*) FROM Goods) + 1,
		'Table',
		(SELECT good_type_id FROM GoodTypes WHERE good_type_name = 'equipment');
```

### UPDATE

```sql
UPDATE имя_таблицы
SET поле_таблицы1 = значение_поля_таблицы1,
    поле_таблицыN = значение_поля_таблицыN
WHERE [условие_выборки]
```

Будьте внимательны, когда обновляете данные. Если вы пропустите оператор WHERE, то будут обновлены все записи в таблице.

### DELETE

```sql
DELETE FROM имя_таблицы
WHERE [условие_отбора_записей];

DELETE имя_таблицы_1 [, имя_таблицы_2] FROM
имя_таблицы_1 JOIN имя_таблицы_2
ON имя_таблицы_1.поле = имя_таблицы_2.поле
[WHERE условие_отбора_записей];

DELETE Reservations FROM
Reservations JOIN Rooms ON
Reservations.room_id = Rooms.id
WHERE Rooms.has_kitchen = false;


TRUNCATE TABLE имя_таблицы; -- быстрый вариант
/* 
    Не срабатывают триггеры, в частности, триггер удаления
    Удаляет все строки в таблице, не записывая при этом удаление отдельных строк данных в журнал транзакций
    Сбрасывает счётчик идентификаторов до начального значения
    Чтобы использовать, необходимы права на изменение таблицы
 */
```


## дата


 * DATE			YYYY-MM-DD
 * DATETIME		YYYY-MM-DD hh:mm:ss
 * TIMESTAMP	YYYY-MM-DD hh:mm:ss
 * TIME			hhh:mm:sss
 * YEAR			YYYY - полный формат
 * YY или Y 	- сокращённый формат, который возвращает год в пределах 2000-2069 для значений 0-69 и год в пределах 1970-1999 для значений 70-99
 * `CURDATE(), CURTIME(), NOW()`
 * 

```sql
CAST("2022-06-16 16:37:23" AS DATETIME)			-- 2022-06-16T16:37:23.000Z
CAST("2014/02/22 16*37*22" AS DATETIME)			-- 2014-02-22T16:37:22.000Z
CAST("20220616163723" AS DATETIME)				-- 2022-06-16T16:37:23.000Z
CAST("2021-02-12" AS DATE)						-- 2021-02-12T00:00:00.000Z
CAST("160:23:13" AS TIME)						-- 160:23:13
CAST("89" AS YEAR)								-- 1989
STR_TO_DATE('November 13, 1998', '%M %d, %Y') 	-- 1998-11-13T00:00:00.000Z
TIMESTAMPDIFF(unit, datetime1, datetime2)		-- разницу между двумя DATE или DATETIME в конкретной единице измерения
```

## Конвертация типов

 * CAST

```sql
DATE		-- "YYYY-MM-DD".
DATETIME	-- "YYYY-MM-DD hh:mm:ss"
TIME		-- "hh:mm:ss".
DECIMAL[(M[,D])]	-- M - максимальное количество знаков до и D - после запятой. По умолчанию, D равен 0, а M равен 10.
CHAR[(N)]	-- можно передать максимальную длину строки.
SIGNED		-- знаковое BIGINT
UNSIGNED	-- беззнаковое BIGINT
BINARY		-- BINARY
YEAR		-- YYYY
```

## оконные функции

```sql

/* 
окно, которое подаётся в оконную функцию, может быть:
    * всей таблицей
    * отдельными партициями таблицы, то есть группой строк на основе одного или нескольких полей
    * или даже конкретным диапазоном строк в пределах таблицы или партиции. Например, мы можем определить окно, которое будет передаваться в оконную функцию, как предыдущая + текущая строка таблицы. И тогда для каждой строки значение агрегатной функции будет подсчитываться по-своему, так как данные, которые поступают в функцию будут динамически меняться от строке к строке. Окно будет как бы «скользить» по таблице.

 */

SELECT <оконная_функция>(<поле_таблицы>)
OVER (
      [PARTITION BY <столбцы_для_разделения>]
      [ORDER BY <столбцы_для_сортировки>]
      [ROWS|RANGE <определение_диапазона_строк>]
)
