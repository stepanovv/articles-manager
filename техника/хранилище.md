# Хранилище

[\[_TOC_]]

## технологии
 
 * дедупликация
 * сжатие данных
	* Эффективность сжатия определяется содержимым файлов и обычно  варьируется  в  значении  от 0,33  до 3 раз.
	* Эффективность дедупликации определяется количеством копий файлов и частоте операций с ними и может легко достигать 10.
	* Сжатие/распаковка происходит практически в режиме реального времени, не требовательно к ресурсам и уменьшает количество операций ввода/вывода.
	* Операция дедупликации очень требовательна к количеству памяти, увеличивает латентность системы и количество операций ввода/вывода.
 * репликация
 * резервирование
 * горячая замена
 * Replication
 * Snapshots
 * Volume Management
 * File System
 * Clustering

## Инструменты тестирования

 * доступность
 * производительность
 * согласованность
 * задержки
 * пропускная способность

## Оптимизация 

 * [Производительность распределенного хранилища: препродакшн тесты 2021](https://habr.com/ru/companies/selectel/articles/547314/)
 * проектирование хранилищ данных и файловых систем [Работа с файлами — это сложно](https://habr.com/ru/post/573706/)
 * common performance
	* [Disk and File System Optimisation](https://shatteredsilicon.net/disk-and-file-system-optimisation/)
	* [Chapter 33. Factors affecting I/O and file system performance](https://access.redhat.com/documentation/ru-ru/red_hat_enterprise_linux/8/html/monitoring_and_managing_system_status_and_performance/factors-affecting-i-o-and-file-system-performance_monitoring-and-managing-system-status-and-performance)
 * ML
	* [Storage Performance Basics for Deep Learning 2018](https://developer.nvidia.com/blog/storage-performance-basics-for-deep-learning/)
 * RAID
	* [ext3 RAID stride calc](https://busybox.net/~aldot/mkfs_stride.html)
	* [RAID-массивы из SSD: тестирование масштабируемости производительности на платформе Intel Z87 FCenter 2013](https://fcenter.ru/online/hardarticles/hdd/36217-RAID_massivy_iz_SSD_testirovanie_masshtabiruemosti_proizvoditel_nosti_na_platforme_Intel_Z87)
 * DB stack
	* [Oracle® Database Performance Tuning Guide 11g Release 2. I/O Configuration and Design](https://docs.oracle.com/cd/E15586_01/server.1111/e16638/iodesign.htm)
	* [mysql Optimizing Disk I/O](https://dev.mysql.com/doc/refman/8.3/en/disk-issues.html)
	* [Linux, Apache, MySQL, PHP Performance End to End 2015](https://books.google.ru/books?id=Z3ciBgAAQBAJ&printsec=frontcover&hl=ru)
 * partition align - @deprecated
	* [Aligning IO on a hard disk RAID - the Theory. Percona lab. 2011](https://www.percona.com/blog/aligning-io-on-a-hard-disk-raid-the-theory/)
	* [Partition Alignment detailed explanation. Thomas Krenn. 2011](https://www.thomas-krenn.com/en/wiki/Partition_Alignment_detailed_explanation)
	* [SSD Alignment Calculator 2009](https://www.techpowerup.com/articles/other/157)
	* [Partition alignment](https://en.wikipedia.org/wiki/Partition_alignment)
	* [gparted alignment](https://gparted.org/display-doc.php?name=help-manual#gparted-specify-partition-alignment)
	* сравнение aligned/unaligned WD дисков [Терабайтные жесткие диски: третий подход FCenter 2010](https://fcenter.ru/online/hardarticles/hdd/28121-Terabajtnye_zhestkie_diski_tretij_podhod)
 * SAN/Enterprise
	* оптимизация ОС [Ceph: A Journey to 1 TiB/s 2024](https://ceph.io/en/news/blog/2024/ceph-a-journey-to-1tibps/)

## Оптимизация стэка хранения

 * ![](./хранилище/storage_tech.jpg)
 * ![](./хранилище/хранилище_уровни.jpg)
 * Данные/Клиент/Сервер
	* алгоритмы синхронизации и репликации
	* алгоритмы нарезки логических блоков
	* алгоритмы сжатия
	* шифрование, аутентификация
 * Сеть, доставка
	* среды/каналы, оборудование, протоколы, маршрутизация, задержки, потери
	* фильтрация и защита
	* прерывания, пакеты в секунду
	* [DNS](https://help.reg.ru/support/dns-servery-i-nastroyka-zony/nastroyka-resursnykh-zapisey-dns/chto-takoye-resursnyye-zapisi-dns)
	* [SDN](https://opennetworking.org/sdn-definition/)
	* CDN
		* [Что такое CDN и как это работает? 2019](https://habr.com/ru/companies/selectel/articles/463915/)
		* CNAME --> CDN provider
		* [GeoDNS](https://jameshfisher.com/2017/02/08/how-does-geodns-work/)
		* AnyCast
 * СУБД
	* Кэш, экстенты, разделы
	* индексы, издержки нормализации, уровни изоляции транзакций
	* табличные пространства, журналирование/логи
	* репликация, кластеризация
 * Виртуализация
	* [виртуальная ФС VMFS](https://www.vmware.com/pdf/esx3_partition_align.pdf)
	* VCS virtual cluster system - виртуализация на уровне драйвера ОС
		* ![](./хранилище/vcs.jpg)
	* размер блока/раздела/экстента
	* репликация, кластеризация
	* шифрование
 * ОС
	* драйвера, кэширование
	* системы защиты и фильтрации
	* swap
 * Файловая система
	* сжатие, снимки, дедупликация, виртуальные тома, журналирование, дефрагментация
	* размер блока/раздела/экстента
	* Idle-Time Garbage Collection, TRIM
	* шифрование
 * Сетевое хранилище
	* NAS - network attached storage
	* SAN - storage area network
	* FAN - FS area network
	* DAS - Direct Attach Storage
	* топология сети, репликация, кластеризация
	* кэш,ЦП,каналы связи
	* дедупликация, сжатие
 * Локальная репликация
 	* RAID(soft/hard)/LVM/Multipath
	* кэш,ЦП
	* размер stripe
	* формула резервирования, уровень RAID
	* шифрование
 * Диски
	* интерфейс - SATA/SAS/SCSI
	* размер, IOPS, throughput
	* геометрия разделов
	* кэш
 	* HDD - размер сектора нивелируются размером цилиндра, чем ближе к шпинделю тем быстрее
	* SSD - размер блока
	* шифрование

## Диски

 * https://www.backblaze.com/cloud-storage/resources/hard-drive-test-data
 * https://www.harddrivebenchmark.net/
 * https://www.backblaze.com/blog/backblaze-drive-stats-for-2023/
 * https://www.backblaze.com/cloud-storage/resources/hard-drive-test-data
 * https://www.tomshardware.com/features/hdd-benchmarks-hierarchy

## RAID

 * https://www.omnicalculator.com/other/raid
 * https://www.enterprisestorageforum.com/management/raid-levels-explained/
 * https://market.marvel.ru/blog/servery/tipy-i-urovni-raid-massivov/
 * https://ru.wikipedia.org/wiki/RAID
 * levels
	* 0
		* n
		* ускорение только для больших файлов
	* 1
		* n/2
	* 5
		* n-1
		* контрольная сумма размазывается по всем дискам
	* 6
		* n-2
		* 2 контрольные суммы размазывается по всем дискам
	* 50
	* 60

## Файловая система

 * kbo/kb/admin/filesystems.md

## СУБД

 * [](./СУБД.md)
