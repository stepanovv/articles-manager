#!/bin/bash

# скрипт проверяет доступность по списку репозиториев, добавляет все новые файлы и отправляет изменения в master из текущей ветки

# usage:
# ./remote_git_init.sh # once
# ./push.sh message

result=""
errcode=0
message='content'
[[ -z $1 ]] || message=$1

push() {

	remote=${1}
	branch=${2}
	[[ -z ${2} ]] && branch=master

	echo "++++++++++PUSH:${remote}:test"

	git remote show ${remote} && {

		echo "++++++++++PUSH:${remote}:start"
		git push ${remote} ${branch}

		errcode=$?
		[[ ${errcode} -ne 0 ]] && result="${result}\n ++++++++++PUSH:${remote}:ERROR:${errcode}"
		[[ ${errcode} -eq 0 ]] && result="${result}\n ++++++++++PUSH:${remote}:OK"

		echo "++++++++++PUSH:${remote}:end"
		return ${errcode}
	} || {
		errcode=$?
		result="${result}\n ++++++++++PUSH:${remote}:FAILED REMOTE TEST:${errcode}"
		echo -e "++++++++++++++++++++++++++++++++++++++++"
		echo -e "\n${result}\n"
		echo -e "++++++++++++++++++++++++++++++++++++++++"
		return ${errcode}
	}
}

git add -A ./
git commit -am ${message}

push "gl" || exit ${errcode}
push "local"
# push "pc-3"
# push "usb"
# push "bb"

#сначала в облако, на сервере потом будем из него качать по хуку
#git push pc-1 master
##теперь запускаем хук
##git push --prune svv master
#git status

echo -e "++++++++++++++++++++++++++++++++++++++++"
echo -e "\n\n${result}\n\n"
echo -e "++++++++++++++++++++++++++++++++++++++++"

#exit ${errcode}
