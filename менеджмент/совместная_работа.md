# Инструменты совместной работы

 * [инструменты индивидуальной работы](../карьера/инструменты.md)

## обзор

 * bitbucket: git+wiki
 * trello бизнес + corello + butler +15$
 * wakatime +5$
 * google doc
 * figma: редактирование, zeplin просмотр
 * discord общение голосом и текстом
 * slack
 * бесплатный хостинг: heroku, netlify
 * zoom.us - видеоконференции

## справочная

| № | ФИО | роль, чем может помочь | город | время MSK+X | рабочее время MSK | на связи MSK | skype/TG | тел(экстренная связь) | email(для сервисов/календарей) | примечание |
| --- | --- 				| --- 		| --- 			| --- | --- | --- | --- 			| --- 			| --- 						| --- 	|
| 1 | Вася | фронтовик(FE), ментор 		| Питер 		| +0 |  9-18 |  9-18 |          	| 8  | @mail.com				| 		|
| 2 | Маша | тыловик(BE), доклады на конфу 		| Казань 		| -3 |  9-18 |  9-18 |          	| 8  | @mail.com				| 	не пишите в личку до обеда	|
| 3 | Петя | девопс, стенды, мониторинг	| Владивосток 		| -6 |  9-18 |  9-18 |          	| 8  | @mail.com				| 		|
| 4 | Коля | лид, зам по странным вопросам  | Москва 		| +0 |  9-18 |  9-18 |          	| 8  | @mail.com				| 	совещания только с 14 до 17	|

* ФИО
* роль, чем может помочь
* город
* время MSK+X
* рабочее время MSK
* на связи MSK
* skype/TG
* тел(экстренная связь)
* email(для сервисов/календарей)
* примечание

## planning poker

 * [тренажёр](https://play.planningpoker.com/plans)
 * [бесплатно тренажёр](http://www.votingpoker.com)
 * [жирный плагин](https://marketplace.atlassian.com/apps/700473/agile-poker-estimation-tool-for-jira)

## online whiteboard

 * miro
 * https://getlocus.io/
 * https://sboard.online/tariffplans
 * https://pruffme.com/pages/prices/
 * https://www.figma.com/figjam/
 * old
	* https://openmeetings.apache.org/#multi-whiteboard-and-chat - его нужно ставить на свой сервер, оно работает, но выглядит не модно
	* https://bigbluebutton.org/teachers/features/ - тоже надо ставить, выглядит помоднее
	* https://www.heyhi.sg/solution/meeting
	* https://padlet.com/dashboard - обмен карточками, каменты, не совсем онлайн

## чат

 * ! обязательно отдельная учётка для работы
 * видео+аудио+текст, 10+ участников, файлы, рабочий стол, запись
 * https://calls.mail.ru/
 * https://telemost.yandex.ru/
 * telegram
 * discord
 * skype
 * https://help.mail.ru/biz/myteam/tariffs
 * https://360.yandex.ru/premium-plans
 * https://support.discord.com/hc/en-us/community/posts/360044645291-Music-in-chat-rooms
 * [slack tips](https://slack.com/intl/en-ru/slack-tips/all)

## онлайн встречи

 * запись встреч
	* obs studio - win/lin/mac
	* windows xbox game bar
 * микрофоны видеокамеры гарнитуры для конференций
	* [Опыт записи подкастов Веб-стандарты и devSchacht 2018](https://www.youtube.com/watch?v=_ChmShmST-s)
	* [Мегаобзор микрофонов для записи подкастов 2015](https://habr.com/ru/company/audiomania/blog/385995/)

## корпоративные порталы

 * https://pryaniky.com/

## задачники

 * kaiten
 * https://moo.team/prices/
 * https://ru.yougile.com/


### trello

 * статьи
	* [советы от timedoctor](https://www.timedoctor.com/blog/how-to-use-trello/)
	* https://trello.com/tour
 * numberstats - суммирование. необходимо проверить, т.к. это сторонний плагин. нужен фильтр по людям и ролям
 * сортировка внутри колод делается плагином butler - это кнопка со скриптами
 * hourlytime - надо попробовать - там есть отчёты в ексель но 5 долл в мес
 * timecamp - учёт времени, на одного бесплатно. Кнопка с таймером в карточку и отдельная вкладка с временем в заголовке
 * фильтрация есть по табличкам(ролям), срокам и персонам
 * роли - таблички(tag)
 * приоритеты/статусы раскладываются по колодам карт. Небытие, долгий ящик(веха), неделя(спринт), сегодня, сейчас(1 карточка максимум).
 * передача карточек через редактирование участников
 * фильтрация по спринтам не нужна, у нас есть доска, в ней всё актуально. Т.е. канбан
 * есть:
	* передача карточек людям
	* группировка по статусу
	* время завершения
	* приоритеты
	* фильтрация по персонам
	* фильтрация по спринтам
	* фильтрация по ролям
	* сортировка по приоритетам
	* суммирование потраченного времени по людям
	* фильтрация по срокам
	* интеграция гит
	* интеграция фигма
	* интеграция диск
	* ссылка на карточку
 * интеграция:
	 * google drive
	 * draw.io
	 * figma
	 * zeplin
	 * timecamp
	 * hangouts chat двунаправленный
	 * gitlab
	 * bitbucket
	 * VSCode через плагин
	 * [slack](https://help.trello.com/article/1049-slack-app)
	 * [timecamp](https://www.timecamp.com/app#/timesheets/timer)
	 * [discord](https://discordbots.org/bot/trello) [](https://www.youtube.com/watch?v=f3HHi7h_R9o)
	 * [api key](https://trello.com/app-key) и нажать там на генерацию токена
	 *
 * planning pocker в каждой карточке
 * есть локальные плагины для IDE https://marketplace.visualstudio.com/items?itemName=mkloubert.vscode-kanban


### Zapier

* 5 минут интервал
* trello->discord
* ```
	{{59072091__card__shortUrl}}
	Карточка: "{{59072091__card__name}}" TId: "{{59072091__card__idShort}}"
	Роли: {{59072091__card__labels[]name}}
	Событие: {{59072091__type}}
	Колода: "{{59072091__data__listBefore__name}}" до: "{{59072091__data__listBefore__name}}" после: "{{59072091__data__listAfter__name}}"
	```
* google calendar - содание мероприятия создаёт карточку

### плагин buttler

* при создании карточки добавлять участников, покер
* при перемещении карточки по колодам назначать дату завершения
* при истечении срока завершения в колодах кроме "готово" делать оповещение или перемещать в другие колоды
* ```
	when a card is added to list "сегодня" by anyone, set due today
	when a card is added to list "на неделе" by anyone, set due the next friday
	when a card is created in the board by anyone, add member @user01165944 to the card, and add the lime "ФРОНТ" label to the card
	when a card is added to list "Долгий ящик" by anyone, remove the due date from the card
	when a card is added to list "небытие" by anyone, remove the due date from the card
	when a card is added to list "Готово" by anyone, remove the due date from the card
	```


### плагин для Sentry

 * [project settings-legacy plugins-trello](https://sentry.io/settings/maiks/projects/angular/plugins/trello/)
 * [trello issues](https://sentry.io/integrations/trello/)
 * https://sentry.io/integrations/
 * [похоже, что интеграция не работает. Кнопки создать карточку нет](https://forum.sentry.io/t/trello-integration-setup/3433)
 * [slack](https://github.com/getsentry/sentry/tree/master/src/sentry/integrations/slack)
 * админка: фильтры, предпросмотр json в списках, события в чятик, создание карточек из событий, сбор системной информации, свои тэги
 * если надо будет вручную писать агент для сбора данных, добавить в него вручную информацию о браузере: user-agent, размер окна
 * ТЗ на админку сервиса логирования ошибок: фильтр по типам, тэгам(desc/lvl/data/...), дате
 * ТЗ на сервис сбора ошибок: приём и разбор по тэгам(desc/lvl/data/...) post запроса с телом json
 * приём сообщений: запрос post, тело json, внутри парсинг по тэгам.
	```js
		error.log({data:this.data, desc:'error happens' });
		error.log({
			data:this.data,
			desc:'error happens',
			lvl: this.error.ELvl.warn,
			src:'src/app/services/error.service.ts:61',
			mtd:'log()',
			task:'0000'
			});
	```

## JIRA

 * [jira](./jira.md)

## retrospective ретроспективы

 * https://www.atlassian.com/ru/team-playbook/plays/retrospective#instructions
 * https://www.funretrospectives.com/
 * https://onlineretrospectives.com/
	* только практики без утилит
 * https://reetro.io/reetro-pricing.html
	* анонимные за деньги
 * https://easyretro.io/pricing
 * https://www.teamretro.com/plans
 * https://lucid.app/ru/pricing/lucidspark#/pricing

## контракты API

* https://github.com/swagger-api/swagger-codegen
* https://app.swaggerhub.com/
* Java server

Лучшим инструментом для редактирования swagger в yaml является плагин для Intellij IDEA под названием Swagger от некоего Zalando и плагин Swagger к плагину Swagger от Zalando.
Это единственный бесплатный плагин поддерживающий спецификацию openapi 3.0.
Но при копирование любых блоков пересчитывайте отступы. Т.к. интеллисенсе в плагине не идеально и он задвигает некоторые блоки на недопустимую глубину. В yaml вложеность блоков прописана на уровне синтаксиса элементов.

Не трогайте SwaggerHub от swagger.io . Если тронули, то неверьте его higlight-ингу - он бежбожно врет, особенно когда выдает странные ошибки "здесь должа быть стринг".
Если и это вас не напугало, то к редактированию документа приступайте, только после верификации email, иначе потеряете все что сделали после 10 минут после начала работы.

После редактирования файла его надо проверять по swagger-ui на валидность. Нужно будет прочитать все созданные методы в swagger-ui и проверить, что метод получает и возвращается все что вы прописали.
После swagger-ui надо проверить на ошибки при помощи генератора сервера и клиента, в процессе генерации они выдают ошибки.


Пример конфигурационного json для бэкенда(gen_opt.json):
```json
{
	"modelPackage":"ru.name.company.services.auth.dto",
	"apiPackage":"ru.name.company.services.auth.api",
	"invokerPackage":"ru.name.company.services.auth",
	"groupId":"ru.name.company",
	"artifactId":"auth",
	"artifactVersion": "0.1-snapshot",
	"localVariablePrefix": "",
	"serializableModel": true,
	"hideGenerationTimestamp": true,
	"java8": true,
	"useBeanValidation": true
}
```

команда по генерации сервера по yaml(при помощи swagger-codegen-cli):
```bash
  java -jar /d/bin/swagger-codegen/modules/swagger-codegen-cli/target/swagger-codegen-cli.jar generate \
  -i company/servers/api/api.yaml \
  -l spring --library spring-mvc \
  -c gen_opt.json \
  -o auth-now2
```

swagger-codegen-cli можно взять с https://github.com/swagger-api/swagger-codegen

## трекеры

### timecamp

 * https://www.timecamp.com/
 * https://appimage.github.io/TimeCampDesktop/
 * `zypper in libappindicator1 libdbus-1-3 libgtk-2_0-0 libindicator7 liblzma5 libnotify4 libsqlcipher-3_20_1-0 libsqlite3-0 libudev1 libX11-6 libXss1 ImageMagick`

### wakatime.com

 * время работы над кодом из плагинов к IDE
 * интеграция гитлаб, бб, гх
 *  https://wakatime.com/
 * открытый аналог локально: https://marketplace.visualstudio.com/items?itemName=hangxingliu.vscode-coding-tracker
 * аналог похуже, нет привязки к веткам и коммитам https://marketplace.visualstudio.com/items?itemName=softwaredotcom.swdc-vscode

