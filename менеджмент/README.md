# Менеджмент, управление другими людьми

 * [найм](./найм.md)
 * [Совместная работа](./совместная_работа.md)
 * [JIRA](./jira.md)
 * [процессы](./процессы.md)
 * [Обучение, наставничество, развитие](./обучение.md)

## TODO разобрать

 * [комната с опускающимся потолком](https://www.youtube.com/watch?v=xwRt1Ozz7cA)
 * [08) Апгрейд канбан — Барабан-буфер-канат (Максим Дорофеев, LeanKanbanRussia-2014)](https://youtu.be/V-1RAneibos?list=PLm6zCN_KJCrX81iojL2lE2gHSbwnQE-QI&t=89)
 * [Максим Дорофеев — Эффективность неэффективности Встреча CodeFreeze в Москве, 10.07.2014](https://youtu.be/XDF02KmgJFE?list=PLm6zCN_KJCrX81iojL2lE2gHSbwnQE-QI&t=1560)
	* https://youtu.be/XDF02KmgJFE?list=PLm6zCN_KJCrX81iojL2lE2gHSbwnQE-QI&t=2121
 * О связи здоровья со сном
 	* [Джо Роган о сне с Мэтью Уокером - текст](https://vk.com/video-170561727_456239490)
 	* [Джо Роган о сне с Мэтью Уокером - видео](https://www.psyshelves.com/psyreviews/joe-rogan-and-matthew-walker)
 * [Принцип экономии мыслетоплива - Максим Дорофеев - РИТ 2017](https://www.youtube.com/watch?v=fWR5SFhBUWc)
 * [Про особенности работы мозга](https://www.youtube.com/watch?v=dOWsSYqxM58)
 * [Воспитание джуниоров. Пирамида потребностей](https://www.youtube.com/watch?v=w5r4x0QN_JY)
 * [Ценности, принципы, процессы в компании](https://www.youtube.com/watch?v=jGgVSNemdgo)
 * [Как я перестал верить технологиям](https://www.youtube.com/watch?v=f4uXBpP_xxY)
 * [5 минут: важность отдыха](https://www.youtube.com/watch?v=iBHr8gKc5L8)
 * [О важности обучения детей в футболе](https://ideanomics.ru/articles/1315)
 * [10 правил разработки](https://medium.com/devschacht/the-cat-with-a-dragon-tattoo-10-things-you-will-eventually-learn-about-javascript-projects-f3848becd460)
 * [Техническая ипотека: что и кому должен тимлид](https://habr.com/ru/company/lamoda/blog/448278/)
 * [Путь к позиции разработчика-сеньора](https://techrocks.ru/2018/03/09/way-to-senior-developer/) [оригинал](https://dev.to/veloceronte/the-road-to-senior-dev--b6k)
 * [Путь к позиции разработчика-сеньора 2](https://techrocks.ru/2018/03/09/way-to-senior-developer-2/) [оригинал](https://dev.to/veloceronte/the-road-to-senior-dev-part-2-3g8p)
 * [Почему Senior Developer'ы не могут устроиться на работу](https://habr.com/ru/post/460901/)
 * [9 ошибок начинающего руководителя](https://ideanomics.ru/articles/17952)
 * [как строить осмысленную жизнь](https://ideanomics.ru/articles/12063)
 * [Взгляд изнутри на удаленную разработку](https://habr.com/ru/post/295500/)
 * [Как помогать идеям рождаться или отладка в реальном времени](https://www.youtube.com/watch?v=a-OyoVcbwWE)
 * [Оценка и прогнозирование проектов (Часть 0: Введение) максим дорофеев](https://www.youtube.com/watch?v=40NRDkgcksI&list=PLm6zCN_KJCrXXiDWoIczR7B9n73wPX2wn)
 * [011. Школа менеджмента — Ненасильственное управление. Григорий Бакунов](https://youtu.be/iYhaK_BpO8w?t=1580)
 * [матрица компетенций](https://github.com/jorgef/engineeringladders/blob/master/Developer.md)
 * Практика без теории:
	- [Земмельвейс Игнац Филипп ИСТОРИЯ МЕДИЦИНЫ](https://www.historymed.ru/encyclopedia/doctors/index.php?ELEMENT_ID=4948)
	- [Карго-культы в айти Vitaly Sharovatov 2020](https://youtu.be/K-dvO2sLaZk?feature=shared)
 * Психология труда:
	- https://core.ac.uk/download/pdf/81696897.pdf
	- https://urait.ru/book/psihologiya-truda-530652
	Физиология труда:
	- https://www.ozon.ru/product/psihofiziologiya-professionalnoy-deyatelnosti-umstvennyy-trud-uchebnoe-posobie-studentam-880854493/
	- http://repo.ssau.ru/bitstream/Uchebnye-izdaniya/Rukovodstvo-po-fiziologii-truda-Elektronnyi-resurs-ucheb-posobie-76477/1/Ведясова%20О.А.%20Руководство.pdf
 * Организационная психология:
	- [Организационная психология А. Л. Свенцицкий 2019](https://urait.ru/book/organizacionnaya-psihologiya-425235)
	- http://elibrary.sgu.ru/uch_lit/2256.pdf
 * Социология труда:
	- https://www.isras.ru/files/File/publ/Toschenko_Zvetkova_soc_truda.pdf
	Теория систем:
	- https://www.amazon.co.uk/Ackoffs-Best-Classic-Writings-Management/dp/0471316342
 * Научпоп про мотивацию:
	- https://www.amazon.com/gp/product/B0033TI4BW
	Теория исследования операций:
	- https://www.amazon.com/Platform-Change-1st-Stafford-Beer-ebook/dp/B0C3XVYVK6
 * Кибернетика:
	- [ Винер Норберт Название: Кибернетика или управление и связь в животном и машине 1961](https://royallib.com/book/viner_norbert/kibernetika_ili_upravlenie_i_svyaz_v_givotnom_i_mashine.html)
	- [The Principles of Product Development Flow: Second Generation Lean Product Development Hardcover - January 1, 2009](https://www.amazon.com/Principles-Product-Development-Flow-Generation/dp/1935401009)
 * [Flow: The Psychology of Optimal Experience Paperback - Bargain Price, July 1, 2008 by Mihaly Csikszentmihalyi (Author)](https://www.amazon.com/Flow-Psychology-Experience-Perennial-Classics/dp/0061339202)
 * https://careerfoundry.com/en/blog/product-management/product-management-books/
    * Dedicated reading time: Set aside regular intervals for reading. Whether it's 20 minutes a day or a couple of hours every weekend, consistency is key
    * Take notes: As you read, jot down key points, ideas, or concepts that resonate with you. This not only helps in retention but also serves as a quick reference in the future.
    * Discuss them: Engage in discussions with fellow product managers or colleagues. Sharing insights and debating concepts can lead to deeper understanding. There are even product book clubs out there that you can join.
    * Implement in real-time: Try to apply the concepts you read about in your day-to-day work. Whether it's a new strategy, a design principle, or a leadership technique, real-world application will solidify your learning.
 * [Сайт Ника Коленды с подборкой научных работ о том, как манипулируют нами](https://www.kolenda.io/guides/advertising)
 * модель мышления Андрея Курпатова
 * Тим Урбан - модель мышления
 * Канеман - думай медленно, решай быстро
 * Atul Gawande - the checklist manifesto
 * Чарльз Дахигг Сила привычки
 * Нассим Талеб Шкура на кону Skin in the game
 * Анатлоий Левенчук - системное мышление
 * [Элегантная головоломка. Системы инженерного менеджмента Уилл Ларсон 2019](https://www.litres.ru/book/uill-larson/elegantnaya-golovolomka-sistemy-inzhenernogo-menedzhmenta-69519343/)
 * [Job and Work Analysis: Methods, Research, and Applications for Human Resource Management 3rd Edition by Frederick P. Morgeson, Michael T. Brannick, Edward L. Levine](https://www.amazon.com/Job-Work-Analysis-Applications-Management-ebook/dp/B07PMTQL2Q/)
 * [Азбука системного мышления Донелла Х. Медоуз](https://www.litres.ru/book/donella-medouz/azbuka-sistemnogo-myshleniya-33388675/)
 * https://www.linkedin.com/advice/0/how-does-ats-score-your-resume-skills-resume-writing
 * [Как хакнуть себе голову. Эффективно переключаем состояния на примере it специалистов. Николай Сенин](https://club.mnogosdelal.ru/post/2492/)
 * Джордж Акерлоф РЫНОК "ЛИМОНОВ": НЕОПРЕДЕЛЕННОСТЬ КАЧЕСТВА И РЫНОЧНЫЙ МЕХАНИЗМ
 	* `/wd1tb-2/doc/books/Джордж Акерлоф РЫНОК "ЛИМОНОВ": НЕОПРЕДЕЛЕННОСТЬ КАЧЕСТВА И РЫНОЧНЫЙ МЕХАНИЗМ.pdf`
 * [Как я работаю со своим списком задач (в Singularity)](https://www.youtube.com/watch?v=HcREd8_MCxY&t=855s)
 * [Ярик Астафьев, Аксель Ткачев: публичное собеседование тимлида](https://www.youtube.com/watch?v=d525op0bq_I)
 * [pair programming](https://www.youtube.com/watch?v=q7d_JtyCq1A)
 * pair testing:
	* https://www.youtube.com/watch?v=jyFNzHFd_80
	* https://www.youtube.com/watch?v=ctRD2KBUYSI
 * культура ведения интервью: https://docs.google.com/document/d/1Cr1J5B19nAIcVIqKEONKvy6ERzcWiQ3wcUL5Qr64DMQ/edit?usp=sharing
 * [ТК РФ Ст. 188. Возмещение расходов при использовании личного имущества работника](https://legalacts.ru/kodeks/TK-RF/chast-iii/razdel-vii/glava-28/statja-188/)
 * [стрессовое интервью](https://www.unisender.com/ru/blog/kuhnya/chto-takoe-stressovoe-intervyu-i-kak-ego-projti/)
 * [Master the Coding Interview: Big Tech (FAANG) Interviews](https://www.udemy.com/course/master-the-coding-interview-big-tech-faang-interviews/)
 * [кусочек фреймворка про софты](https://t.me/c/2125091162/568)
 * [#0 - Effective Altruism, X-risk, (e/acc)](https://reasonisfun.podbean.com/e/0-effective-altruism-x-risk-eacc/)
 * https://open.spotify.com/artist/7y97mc3bZRFXzT2szRM4L4?si=QoEXecpqRlin4ebAGzuvlQ
 * [дообучение после учёбы](https://t.me/mash_tech/723)
 * [Process Decision Record — простой инструмент постепенной рационализации процессов /Виталий Шароватов](https://www.youtube.com/watch?v=1gKdzgjRirk)
 * [Работа с инновациями и рацпредложениями](https://www.youtube.com/watch?v=FHu6lyngi1g)
 * elegant puzzle


