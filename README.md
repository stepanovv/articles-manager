# База знаний руководителя группы разработки

 * мои заметки, статьи, конспекты
 * [менеджмент](./менеджмент/README.md)
 * [техника](./техника/README.md)
	* [типовые проекты](./техника/типовые_проекты.md)
 * [карьера](./карьера/README.md)
 * [разбор интервью/докладов](./интервью/README.md)
 * [статьи](./статьи/README.md)

## Ссылки

 * https://github.com/sharovatov/teamlead
 * https://yandex-team.notion.site/
 * http://links.mnogosdelal.ru/
 * https://www.modernsd.org/content/category/%D0%B2%D1%81%D1%82%D1%80%D0%B5%D1%87%D0%B8/

## статистика

```bash
# начало - февраль
git log --reverse --pretty=oneline --format='DEV: %cd #%h %s' --date=format:'%c' | head -1
# DEV: Чт 02 ноя 2023 17:48:06 #7e14d75 Initialized from 'Pages/Plain HTML' project template
# DEV: Чт 15 фев 2024 15:53:58 #56afa97 Add LICENSE
# DEV: Чт 15 фев 2024 19:52:59 #ded4f14 init

# конец - май, итого 3 месяца

# строк текста
grep --include=\*\.{md,sh} -R '' . | wc -l
# 5853

# машинописных страниц - 5853/30=195
# pdf страниц - 5853/25=234

```

## Плагины

 * [материалы](./техника/типовые_проекты/смарт-картинг.md) просматриваются и редактируются в любом IDE, у меня VSCode
 * ниже список плагинов и утилит, которыми я пользуюсь наряду со [встроенными](https://code.visualstudio.com/Docs/languages/markdown) функциями IDE
 * отдельный [редактор](https://www.yworks.com/products/yed/download) для просмотра `*.graphml` файлов
 * [ERD Editor](https://marketplace.visualstudio.com/items?itemName=dineug.vuerd-vscode)
 * [PlantUML preview](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml)
	* для плагина plantuml нужен [nvm+node+npm](https://nodejs.org/en/download/package-manager) и [openjdk/jre](https://www.microsoft.com/openjdk) или [JDK/jre](https://www.oracle.com/java/technologies/downloads/)
 * [Markdown PDF](https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf)
 * [Markmap - mindmap preview](https://marketplace.visualstudio.com/items?itemName=gera2ld.markmap-vscode)
 * [Print](https://marketplace.visualstudio.com/items?itemName=pdconsec.vscode-print)
 * [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
 * [Russian - Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-russian)
 * [Markdown Table Prettifier](https://marketplace.visualstudio.com/items?itemName=darkriszty.markdown-table-prettify)
 * [Todo MD](https://marketplace.visualstudio.com/items?itemName=usernamehw.todo-md)
 * [OpenAPI (Swagger) Editor](https://marketplace.visualstudio.com/items?itemName=42Crunch.vscode-openapi)
 * 

```bash
npm i
code --list-extensions >> .vs_code_extensions_list.txt
code --list-extensions | xargs -n 1 code --uninstall-extension
cat .vs_code_extensions_list.txt | xargs -n 1 code --force --install-extension
#Windows %APPDATA%\Code\User\settings.json
#macOS $HOME/Library/ApplicationSupport/Code/User/settings.json
#Linux $HOME/.config/Code/User/settings.json
```
 * [.vs_code_extensions_list.txt](./.vs_code_extensions_list.conf)

## TODO

 * [менеджмент](./менеджмент/README.md#todo-разобрать)
 * [техника](./техника/README.md#todo)
 * [веб-версия БЗ](./public/index.html)