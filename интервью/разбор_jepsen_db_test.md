# Jepsen framework


 * [Jepsen 9: A Fsyncing Feeling • Kyle Kingsbury • GOTO 2018](https://www.youtube.com/watch?v=tRc0O9VgzB0)
 * тестирование распределённых систем
 	* определение границ неопределённости
	* генератор операций
	* проигрывание
	* проверка консистентности состояний
	* имитация отказов
 * DB failures
	* split brain
	* broken foreign keys
	* r/w anomalies
	* stale reads
	* dirty reads
 * hazelcast in-memory data grid


