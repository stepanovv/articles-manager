#!/bin/bash

# удаляем стандартный origin, чтобы использовать скрипты pull.sh+push.sh для несокльких реп

repo_name=articles-manager
# host1=pc-3
local_path="/doc/coding/BARE/${repo_name}"

git remote
cat .git/config | grep -i url

git remote add gl git@gitlab.com:stepanovv/${repo_name}.git && git remote remove origin
git branch --set-upstream-to=gl/master master

# ping -c3 -q ${host1} || echo "WARN: ${host1} offline"
# git remote add ${host1} ssh://bsk@${host1}/doc/coding/BARE/${repo_name} && git remote remove origin
# git branch --set-upstream-to=${host1}/master master

[[ -e ${local_path} ]] || {
	echo "ERROR: ${local_path} not exists, fix path or clone bare repo"
	exit 1
}

git remote add local /${local_path}

#git remote add bb git@bitbucket.org:stepanovvgm/${repo_name}.git
#git remote add usb //run/media/bsk/SD29G_EXT4/git-bare/${repo_name}

git remote